from numpy import *


class AggregatedEvent:
    """
    An aggregated event is an event related to one of the appliances and defined by characteristics (its arguments)
    relevant for the classification of the event itself
    """

    def __init__(self, duration, consumption, max_consumption, average_consumption, variance):
        """

        Args:
            duration: interval of time in which water consumption is different from zero
            consumption: consumption/second * duration
            max_consumption: highest consumption in the event's duration
            average_consumption: ""
            variance: ""
        """
        self.duration = duration
        self.consumption = consumption
        self.max_consumption = max_consumption
        self.average_consumption = average_consumption
        self.variance = variance


def aggregate_events(house):
    """
    Args:
        house: the house in which the events take place

    Returns:
        appliances_events: dictionary containing the events for each appliance in the given house

    """
    appliances_events = dict()
    appliances_events['shower'] = aggregate(house.normal_shower)
    appliances_events['faucet'] = aggregate(house.normal_faucet)
    appliances_events['dishwasher'] = aggregate(house.normal_dishwasher)
    if house.is_efficient():
        appliances_events['toilet'] = aggregate(house.efficient_toilet)
        appliances_events['clothes_washer'] = aggregate(house.efficient_clothes_washer)
    else:
        appliances_events['toilet'] = aggregate(house.normal_toilet)
        appliances_events['clothes_washer'] = aggregate(house.normal_clothes_washer)
        appliances_events['bathtub'] = aggregate(house.bathtub)

    return appliances_events


def aggregate(output_trajectory):
    """

    Args:
        output_trajectory: list of all the values per second related to an appliance

    Returns:
        events: list of aggregated events related to an appliance

    """
    # Add a 0 at the beginning and at the end in order to read all the values with the following easy procedure.
    output_trajectory = output_trajectory + [0]
    output_trajectory = [0] + output_trajectory

    events = []

    e = []
    for i in range(1, len(output_trajectory)):

        if output_trajectory[i] != 0 and output_trajectory[i - 1] == 0:
            e = []

        if output_trajectory[i] != 0:
            e.append(output_trajectory[i])

        elif output_trajectory[i] == 0 and output_trajectory[i - 1] != 0:
            events.append(
                AggregatedEvent(len(e), sum(e), amax(e), average(e), var(e)))

    return events
