import logging
import os
import shutil
import sys

import numpy as np
from pyspark import SparkContext, SparkConf
from pyspark.mllib.evaluation import MulticlassMetrics
from pyspark.mllib.tree import DecisionTree
from pyspark.mllib.util import MLUtils

from logger import configure_logger
from config import aggregated_data_path, spark_master_address

TRAINING_PERCENTAGE = 50


def main():
    """
    Trains the spark decision tree classifier with data in SVM format and
     generates the tree model and the confusion matrix
    """
    configure_logger()

    folder_name = '../out/' + str(TRAINING_PERCENTAGE)

    if os.path.isdir(folder_name):
        print 'The previous folder will be deleted. Press enter to continue'
        raw_input()
        shutil.rmtree(folder_name)

    os.makedirs(folder_name)

    logging.info('Initializing Spark Context')
    conf = SparkConf().setAppName("valencia-waters").setMaster(spark_master_address)
    sc = SparkContext(conf=conf)

    # Load and parse the data file into an RDD of LabeledPoint.
    logging.info('Loading SVM Files')
    data_list = list()
    for root, dirs, files in os.walk(aggregated_data_path):
        for f in files:
            if f.startswith('outHouse'):
                sys.stdout.write('\r' + 'Loading:' + str(f))
                data_list.append(MLUtils.loadLibSVMFile(sc, aggregated_data_path + str(f)))

    logging.info('Unifying all data into one RDD')
    data = sc.union(data_list)

    # Split the data into training and test sets (30% held out for testing)
    logging.info('Splitting Data')
    (training_data, test_data) = data.randomSplit([TRAINING_PERCENTAGE / 100.0, (100 - TRAINING_PERCENTAGE) / 100.0])

    logging.info('Saving Data to File')
    training_data.saveAsPickleFile(folder_name + '/TRAINING_DATA')
    test_data.saveAsPickleFile(folder_name + '/TEST_DATA')

    # Train a DecisionTree model.
    # Empty categoricalFeaturesInfo indicates all features are continuous.
    logging.info('Training the Model')
    model = DecisionTree.trainClassifier(training_data, numClasses=6, categoricalFeaturesInfo={}, impurity='gini',
                                         maxDepth=5, maxBins=32)

    logging.info('Saving the Model')
    model.save(sc, folder_name + "/DECISION_TREE_MODEL")

    logging.info('Testing the Model')
    predictions = model.predict(test_data.map(lambda x: x.features))
    labels_and_predictions = test_data.map(lambda lp: lp.label).zip(predictions)
    test_err = labels_and_predictions.filter(lambda (v, p): v != p).count() / float(test_data.count())
    print('Test Error = ' + str(test_err))

    logging.info('Learned classification tree model:')
    print(model.toDebugString())

    logging.info("Generating Metrics")
    metrics = MulticlassMetrics(labels_and_predictions)

    logging.info("Generating Confusion matrix")
    confusion_matrix = metrics.confusionMatrix().toArray()
    np.savetxt(folder_name + '/CONFUSION_MATRIX.txt', confusion_matrix, delimiter='\t', fmt='%d')
    print confusion_matrix

    print 'Press a key to exit'
    raw_input()


if __name__ == "__main__":
    main()
