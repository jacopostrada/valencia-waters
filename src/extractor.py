import json
import logging
import re
from os import listdir
from os import walk

from aggregation import *
from house import *
from logger import configure_logger
from config import data_path, aggregated_data_path

efficiency_level_list = io.loadmat(data_path + 'EfficiencyLevel.mat')['EfficiencyLevel'][0]
number_regex = re.compile("[0-9]+")
number_of_house = 2000


def main():
    """
    Creates house classes from *.mat files and then saves houses and their aggregated events to SVM files
    """
    for root, dirs, files in walk(data_path):

        configure_logger()

        files = sorted(files)
        i = 0
        for f in files:
            if f.startswith('out'):
                house_number = int(re.search(number_regex, f).group(0))
                logging.info('Loading House ' + str(house_number))
                if check_next_house_present(house_number):
                    i += 1
                    continue
                if efficiency_level_list[house_number - 1]:
                    h = EfficientHouse(data_path + f)
                else:
                    h = NormalHouse(data_path + f)

                save_aggregated_data_to_svm(f, h)
                i += 1
                if i >= number_of_house:
                    break


def check_next_house_present(house_number):
    """

    Args:
        house_number: number of the current house

    Returns:
        whether the next house is present or not

    """
    next_house = "outHouse" + str(house_number + 1) + "_90days.txt"
    return next_house in listdir(aggregated_data_path)


def save_aggregated_data_to_svm(f, h):
    """

    Args:
        f: *.mat file
        h: house
    """
    class_mapping = {'shower': 0, 'faucet': 1, 'dishwasher': 2, 'toilet': 3, 'clothes_washer': 4, 'bathtub': 5}
    feature_mapping = {'duration': 1, 'consumption': 2, 'max_consumption': 3, 'average_consumption': 4,
                       'variance': 5}
    aggregated_data = aggregate_events(h)
    svm_file_name = f[:-3] + 'txt'
    with open(aggregated_data_path + svm_file_name, 'w') as outfile:
        for appliance in aggregated_data.keys():
            for aggregated_event in aggregated_data[appliance]:
                svm = str(class_mapping[appliance]) + " "
                for feature_name in sorted(feature_mapping, key=feature_mapping.get):  # loop on class attributes
                    svm += str(feature_mapping[feature_name]) + ":" + str(aggregated_event.__dict__[feature_name]) + " "
                outfile.write(svm + "\n")


if __name__ == "__main__":
    main()


# Not used anymore ...

def json_serializer(o):
    """

    Args:
        o:

    Returns:

    """
    return o.__dict__


def save_aggregated_data_to_json(f, h):
    """

    Args:
        f:
        h:
    """
    aggregated_data = aggregate_events(h)
    json_file_name = f[:-3] + 'json'
    with open(aggregated_data_path + json_file_name, 'w') as outfile:
        json.dump(aggregated_data, outfile, default=json_serializer, indent=4)
