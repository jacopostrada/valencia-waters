from abc import ABCMeta, abstractmethod
from scipy import io

OUTPUT_TRAJECTORY = 'outputTrajectory'


class House:
    """
    This class represents an house with given output trajectories related to the house's appliances
    """
    __metaclass__ = ABCMeta

    def __init__(self, file_path):
        """

        Args:
            file_path: path of the *.mat file containing the house's output trajectories
        """
        self.file_path = file_path
        self.mat = io.loadmat(self.file_path)

        self.normal_shower = self.extract_array('NormalShower')
        self.normal_faucet = self.extract_array('NormalFaucet')
        self.normal_dishwasher = self.extract_array('NormalDishwasher')

        self.total = self.extract_array('TOTAL')

    def extract_array(self, field_name):
        """

        Args:
            field_name: name of the appliance

        Returns:
            list of appliance's values

        """
        return self.mat[OUTPUT_TRAJECTORY][field_name][0][0][0]

    @abstractmethod
    def is_efficient(self): pass


class EfficientHouse(House):
    """
    An efficient house has efficient toilets and clotheswhashers and no bathtub
    """

    def __init__(self, file_path):
        """

        Args:
            file_path: path of the *.mat file containing the house's output trajectories
        """
        House.__init__(self, file_path)
        self.efficient_toilet = self.extract_array('EfficientToilet')
        self.efficient_clothes_washer = self.extract_array('EfficientClothesWasher')

    def is_efficient(self):
        """

        Returns: always true

        """
        return True


class NormalHouse(House):
    """
    A normal house has normal toilets and clotheswashers and bathtubs
    """

    def __init__(self, file_path):
        """

        Args:
            file_path: path of the *.mat file containing the house's output trajectories
        """
        House.__init__(self, file_path)
        self.normal_toilet = self.extract_array('NormalToilet')
        self.normal_clothes_washer = self.extract_array('NormalClothesWasher')
        self.bathtub = self.extract_array('Bathtub')

    def is_efficient(self):
        """

        Returns: always false

        """
        return False
